# my_tech_test

Improvements:
- Separate terraform modules for each layer of the stack - NLB (module 1) & MQTT server resources (module 2).
- Variables file per region. 
- Use Git community modules where applicable.
- Tag releases to code with versions, in Git.
- Build AMIs using packer and config management (ansible in this case) before building a stack to reduce boot time.
- All other improvements to code are included as comments in the TF files.
