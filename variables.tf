/* Terraform Variables */

variable "region" {
  default = "eu-west-1"
}

variable "certificate_arn" {}
variable "vpc_id" {}
variable "subnet_id" {}
