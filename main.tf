/* Cloud Provider */

provider "aws" {
  region = "${var.region}"
}

/* Terraform Datasources */

data "aws_vpc" "test_stack_vpc" {
  id = "${var.vpc_id}"
}

data "aws_subnet" "test_stack_subnet" {
  id = "${var.subnet_id}"
}

data "aws_ami" "test_stack_base_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }
  filter {
    name   = "block-device-mapping.volume-type"
    values = ["gp2"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  /* Terraform Resources */

  resource "aws_lb" "test_stack_nlb" {
    name                             = "test-stack-nlb"
    internal                         = false
    load_balancer_type               = "network"
    subnets                          = ["${data.aws_subnet.test_stack_subnet.*.id}"]
    enable_deletion_protection       = true
    enable_cross_zone_load_balancing = true //Enables lb in another AZ if the current AZ becomes unavailable

    tags = {
      Environment = "Sandbox"
    }
  }

  resource "aws_lb_target_group" "test_stack_nlb_tg" {
    name     = "test-stack-nlb-tg"
    port     = 8883
    protocol = "TCP"
    vpc_id   = "${data.aws_vpc.test_stack_vpc.id}"
  }

  resource "aws_lb_listener" "test_stack_nlb_listener" {
    load_balancer_arn = "${aws_lb.test_stack_nlb.arn}"
    port              = "443"
    protocol          = "TLS" //Terminate TLS on the NLB and forward requests coming through, to 8883 on Target Group
    ssl_policy        = "ELBSecurityPolicy-2016-08"
    certificate_arn   = "${var.certificate_arn}"

    default_action {
      type             = "forward"
      target_group_arn = "${aws_lb_target_group.test_stack_nlb_tg.arn}"
    }
  }

  resource "aws_launch_configuration" "test_stack_lc" {
    name_prefix          = "test-stack-lc-"
    image_id             = "${data.aws_ami.test_stack_base_ami.id}"
    instance_type        = "t2.micro"
    security_groups      = ["${aws_security_group.test_stack_sg.id}"]
    iam_instance_profile = "${aws_iam_instance_profile.ec2_s3_access_profile.name}"
    user_data            = <<EOF
#!/bin/sh
/* Install MQTT using ansible playbook - Assuming Ansible is installed on the base AMI */
/* Assuming copy to local home and clean-up after installation */
ansible-playbook mqtt_ansible_playbook/playbooks/mqtt_installation.yml
EOF

    lifecycle {
      create_before_destroy = true
    }
  }

  resource "aws_autoscaling_group" "test_stack_asg" {
    name                 = "test-stack-asg"
    launch_configuration = "${aws_launch_configuration.website_launch_config.name}"
    min_size             = 1
    max_size             = 1
    target_group_arns    = ["${aws_lb_target_group.test_stack_nlb_tg.arn}"]
    vpc_zone_identifier  = ["${data.aws_subnet.test_stack_subnet.*.id}"]

    lifecycle {
      create_before_destroy = true
    }

    tag {
      key                 = "Name"
      value               = "Test Instance"
      propagate_at_launch = true
    }
  }

  resource "aws_security_group" "test_stack_sg" {
    vpc_id = "${data.aws_vpc.test_stack_vpc.id}"

    ingress {
      from_port   = 8883
      to_port     = 8883
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress = [
      {
        from_port   = 8883
        to_port     = 8883
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        from_port   = 80
        to_port     = 80
        protocol    = "HTTP"
        cidr_blocks = ["0.0.0.0/0"] // Allow downloads from internet
      },
      {
        from_port   = 443
        to_port     = 443
        protocol    = "HTTPS"
        cidr_blocks = ["0.0.0.0/0"] // Allow downloads from internet
      }
    ]

    tags {
      Name = "test-stack-ec2-security-group"
    }
  }

  /* Cloudwatch alarm to trigger when CPU utilization is in 'insufficient data' */
  resource "aws_cloudwatch_metric_alarm" "test_stack_cwalarm" {
    alarm_name                = "mqtt_server_down"
    comparison_operator       = "GreaterThanOrEqualToThreshold"
    evaluation_periods        = "2"
    metric_name               = "CPUUtilization"
    namespace                 = "AWS/EC2"
    period                    = "120"
    statistic                 = "Average"
    threshold                 = "90"
    alarm_description         = "Monitors ec2 cpu utilization and also sends an SNS notification when the server becomes unavailable (insufficient_data)"
    alarm_actions             = ["${aws_sns_topic.test_stack_sns_cpu_overutilization.arn}"]
    insufficient_data_actions = ["${aws_sns_topic.test_stack_sns_insufficient_data.arn}"]
  }

  /* SNS topics that gets triggered when the Cloudwatch alarm goes into ALARM or INSUFFICIENT_DATA state */
resource "aws_sns_topic" "test_stack_sns_cpu_overutilization" {
  name = "user-updates-topic"
}

resource "aws_sns_topic_subscription" "test_stack_sns_overutilization_target" {
  topic_arn = "${aws_sns_topic.test_stack_sns_cpu_overutilization.arn}"
  protocol  = "sms" /* Ideally email if available, currently unsupported by TF */
  endpoint = "<sms_endpoint>" /* Configure SMS endpoint */
}

resource "aws_sns_topic" "test_stack_sns_insufficient_data" {
  name = "user-updates-topic"
}

resource "aws_sns_topic_subscription" "test_stack_sns_insufficient_data_target" {
  topic_arn = "${aws_sns_topic.test_stack_sns_insufficient_data.arn}"
  protocol  = "sms" /* Ideally email if available, currently unsupported by TF */
  endpoint  = "<sms_endpoint>" /* Configure SMS endpoint */
}
