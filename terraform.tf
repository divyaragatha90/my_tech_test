/* Terraform version and state file location */
terraform {
  required_version = "= 0.11.14"
  backend "s3" {}
}
